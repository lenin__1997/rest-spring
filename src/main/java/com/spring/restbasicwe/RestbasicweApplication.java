package com.spring.restbasicwe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestbasicweApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestbasicweApplication.class, args);
	}

}
